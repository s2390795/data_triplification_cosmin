import sys
import pandas as pd
from rdflib import Graph, Literal, Namespace, RDF, URIRef
from rdflib.namespace import XSD


def is_number(value):
    try:
        float(value)
        return True
    except ValueError:
        return False


def main(csv_file_path):
    # Read the dataset
    df = pd.read_csv(csv_file_path)

    # Define the namespaces
    SAREF = Namespace("https://saref.etsi.org/core/")
    EX = Namespace("http://example.org/")

    # Create a new RDF graph
    g = Graph()

    # Bind namespaces
    g.bind("saref", SAREF)
    g.bind("ex", EX)

    # iterate over whole dataset
    for index, row in df.iterrows():
        timestamp = row['utc_timestamp']
        # skip first 2 columns since they're timestamps
        for col in df.columns[2:]:
            value = row[col]
            if pd.notna(value) and is_number(value):
                device_id = URIRef(EX[f"Device/{col}"])
                measurement_id = URIRef(EX[f"Measurement/{index}_{col}"])

                # add the device
                g.add((device_id, RDF.type, SAREF.Device))

                # add the measurement
                g.add((measurement_id, RDF.type, SAREF.Measurement))
                g.add((measurement_id, SAREF.hasTimestamp, Literal(timestamp, datatype=XSD.dateTime)))
                g.add((measurement_id, SAREF.hasValue, Literal(float(value), datatype=XSD.float)))
                g.add((measurement_id, SAREF.isMeasuredBy, device_id))

    # make turtle file with all 
    g.serialize(destination='graph.ttl', format='turtle')


if __name__ == "__main__":
        main(sys.argv[1])
